import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { AngularFirestore } from 'angularfire2/firestore';


@Injectable()
export class FirebaseService {
    shoppingListRef$: Observable<any[]>;

    constructor(public firestore: AngularFirestore,public afd: AngularFireDatabase){
        this.shoppingListRef$ = this.afd.list('shopping-list').valueChanges();;
        console.log("CHUUUUUUUUUUUUUUUUUUUUUUUU");
    }

    getShoppingItems() {
        return this.afd.list('/utilisateurs');
    }

    addItem(name) {
        this.afd.list('/utilisateurs').push(name);
    }

    removeItem(id){
        this.afd.list('/utilisateurs/').remove(id);
    }

    
  createSong(
    songName: string
  ): Promise<void> {
    const id = this.firestore.createId();

    return this.firestore.doc(`songList/${id}`).set({
      id,
      songName,
    });
  }
}