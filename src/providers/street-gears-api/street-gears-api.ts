import { HttpClient } from '@angular/common/http';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class StreetGearsApiProvider {
  private baseUrl = "https://projet-services-mobile.firebaseio.com/"

  constructor(public http: Http) {
    console.log('Hello StreetGearsApiProvider Provider');
  }
  
  getUtilisateurs() {
    return new Promise(resolve => {
      this.http.get(`${this.baseUrl}/utilisateurs.json`).subscribe(res => resolve(res.json()));
    });
  }

  getTrajets(){
    return new Promise(resolve => {
      this.http.get(`${this.baseUrl}/trajets.json`).subscribe(res => resolve(res.json()));
    });
  } 

  getEtapes(){
    return new Promise(resolve => {
      this.http.get(`${this.baseUrl}/etapes.json`).subscribe(res => resolve(res.json()));
    });
  }

  getEtapesUnTrajet(trajet: string){
    return new Promise(resolve => {
      this.http.get(trajet+`.json`).subscribe(res => resolve(res.json()));
    });
  }


}
