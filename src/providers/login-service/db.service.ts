import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { TrajetItem } from './../../models/trajet.interface';

@Injectable()
export class DbService {

    private trajetListRef = this.db.list<TrajetItem>('trajet');

    private trajet: TrajetItem = {
        auteur: '',
        description: '',
        heureArrivee: '',
        heureDepart: '',
        nbPlaces: '',
        villeArrivee: '',
        villeDepart: ''
    }

    constructor(private db: AngularFireDatabase) {}

    getListeTrajets() {
        return this.trajetListRef;
    }

    filterByString(traj: TrajetItem) {
        return this.db.list('/trajets', ref => ref.orderByChild('heureDepart').equalTo(traj.heureDepart));
    }

    deleteTrajet(trajet: TrajetItem){
        return this.trajetListRef.remove(trajet.key);
    }

    addTrajet(trajet: TrajetItem) {
        return this.db.list('trajet').push(trajet);
    }

    getTrajetList() {
        return this.trajetListRef;
    }

    editTrajet(trajet: TrajetItem) {
        return this.trajetListRef.update(trajet.key, trajet);
    }
}
