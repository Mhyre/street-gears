import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { TrajetItem } from '../../models/utilisateur.interface';
import { DbService } from '../../providers/login-service/db.service';

@Component({
  selector: 'page-reserver-trajet',
  templateUrl: 'reserver-trajet.html', 
})
export class ReserverTrajetPage {
  private trajet: TrajetItem;

  constructor(public navCtrl: NavController, public navParams: NavParams, private trajets: DbService) {
    this.trajet = this.navParams.data;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReserverTrajetPage');
  }
}
