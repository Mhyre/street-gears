import { Component } from '@angular/core';
import { NavController, NavParams, LoadingOptions, LoadingController } from 'ionic-angular';
import { ReserverTrajetPage } from '../reserver-trajet/reserver-trajet';
import { StreetGearsApiProvider } from '../../providers/street-gears-api/street-gears-api';


@Component({
  selector: 'page-recherche',
  templateUrl: 'recherche.html',
})
export class RecherchePage {
  public recherche: any = {};
/*
  public trajets = [
    { id: 1, avatar: 'assets/imgs/theo.png', nom: 'Theo LeBoulet', description: 'Je fais le trajet avec mon caillou domestique pour aller à une convention furry. Ne vous inquiétez pas il est très sympathique et ne mort pas. Au contraire, cet adorable métagabbro de subduction, avec ses phelspath plagioclase, son olivine et son pyroxène indique son comportement calme et affectueux. Relique d une décompression adiabatique avec des auréoles de hornblende et de chlorite hydroxylée, vous ne pourrez vous empêcher d apprécier sa compagnie.', heureDepart: '15h', heureArrivee: '18h', cout: '7.50€'},
    { id: 2, avatar: 'assets/imgs/madeline.png', nom: 'Madeline', description: 'Coucou, moi je suis normale. Voilà', heureDepart: '15h', heureArrivee: '18h', cout: '8€'},
    { id: 3, avatar: 'assets/imgs/oldhag.png', nom: 'Granny', description: 'J\'ai mal à ma sciatique alors je conduis prudemment', heureDepart: '18h', heureArrivee: '04h', cout: '2€'}
  ];
*/
 public trajets: any;
 public utilisateurs: any;


  constructor(public navCtrl: NavController, 
    public navParams: NavParams, 
    public streetGearsApi: StreetGearsApiProvider,
    public loadingController: LoadingController) {
    this.recherche = this.navParams.data;
  }

  goToReserverTrajet($event, trajet){
    this.navCtrl.push(ReserverTrajetPage, trajet);
  }

  ionViewDidLoad(){
    let loader = this.loadingController.create({
      content: 'Les informations sont sur la route. Veuillez patienter...'
    });

    loader.present().then(() => {
      this.streetGearsApi.getTrajets().then(data => this.trajets = data);
      this.streetGearsApi.getUtilisateurs().then(data => { 
        this.utilisateurs = data;
        loader.dismiss();

        console.log(data);
      });
      
    })
 }

}
