import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { ConnectionPage } from '../connection/connection';
import { LoginServiceProvider } from '../../providers/login-service/login-service'

/**
 * Generated class for the ComptePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-compte',
  templateUrl: 'compte.html',
})
export class ComptePage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController, public loginService: LoginServiceProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ComptePage');
    this.loginModal();
  }

  loginModal() {
    console.log('Ouverture page modal');
    if(this.loginService.authStatus == false)
    {
      var pageConnection = this.modalCtrl.create(ConnectionPage);
      pageConnection.present();
    }
    
  }

}
