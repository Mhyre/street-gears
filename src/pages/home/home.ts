import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { RecherchePage } from '../recherche/recherche';
import { AngularFireList, AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { StreetGearsApiProvider } from '../../providers/street-gears-api/street-gears-api';
import { Http } from '@angular/http';
import { TrajetItem } from '../../models/trajet.interface';
import { DbService } from '../../providers/login-service/db.service'

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  shoppingListRef$: Observable<any[]>;
  newItem = '';
  villeDepart: string = "";
  villeArrivee: string = "";
  dateDepart: string = "";
  recherche: string = ""; 
  public trajets: any;
  test: AngularFireList<any>;
  traj: TrajetItem = {
    auteur: 'assets/imgs/3.png',
    description: 'test',
    heureArrivee: '14h',
    heureDepart: '18h',
    nbPlaces: '2',
    villeArrivee: 'Clermont',
    villeDepart: 'Vannes'
  }

  constructor(public navCtrl: NavController, 
    public afd: AngularFireDatabase, 
    public streetGearsApi: StreetGearsApiProvider,
    private dbservice: DbService) {
//      this.test = afd.list('/utilisateurs').valueChanges();
  }

  goToRecherche($event, villeDepart, villeArrivee, dateDepart){
    this.recherche = "Voyages de " + villeDepart + " à " + villeArrivee + " le " + dateDepart;
    this.navCtrl.push(RecherchePage, this.recherche);
  }

  ionViewDidLoad(){
    this.streetGearsApi.getUtilisateurs().then(data => this.trajets = data);
  }

  addTrajet(traj: TrajetItem) {
    console.log(traj);
    this.dbservice.addTrajet(traj).then(ref => {
      this.navCtrl.push(HomePage);

    });
  }

}
