import { Component } from '@angular/core';
import { ViewController, LoadingController, AlertController } from 'ionic-angular';
import { LoginServiceProvider } from '../../providers/login-service/login-service'

/**
 * Generated class for the ConnectionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-connection',
  templateUrl: 'connection.html',
})
export class ConnectionPage {

  input_username: any;
  input_password: any;
  loading: any;

  constructor(public viewCtrl: ViewController, public loadingCtrl: LoadingController, public loginService: LoginServiceProvider, public alertCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConnectionPage');
  }

  connection(){
    var username: String;
    var password: String;    

    username = this.input_username;
    password = this.input_password;

    console.log("username : " + username);
    console.log("password : " + password);

    this.launchAuthentification(username, password);

    if(username == undefined && password == undefined)
    {
      this.loading = this.loadingCtrl.create({
        content: 'Please wait...',
        duration: 3000,
        dismissOnPageChange: true
      });
      this.loading.onDidDismiss(() => { this.viewCtrl.dismiss(); });
      this.loading.present();
      //this.viewCtrl.dismiss();
    }
  }

  async launchAuthentification(username, password){
    var statusAuthentification = this.loginService.authentification(username, password);
    
    this.loading.dismiss();
    if(!statusAuthentification){
      let alert = this.alertCtrl.create({
        title: 'Error login',
        subTitle: 'Une erreur est survenu (voir Xamarin pour plus d\'informations',
        buttons: ['Pas le choix']
      });
  
      alert.present();
    }
    
  }

}
