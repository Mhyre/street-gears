export interface TrajetItem {
    key?: string,
    auteur: string;
    description: string;
    heureArrivee: string;
    heureDepart: string;
    nbPlaces: string;
    villeArrivee: string;
    villeDepart: string;
}