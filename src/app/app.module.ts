import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { HttpModule } from '@angular/http';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireModule } from 'angularfire2';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ComptePage } from '../pages/compte/compte';
import { HistoriquePage } from '../pages/historique/historique';
import { RecherchePage } from '../pages/recherche/recherche';
import { ReserverTrajetPage } from '../pages/reserver-trajet/reserver-trajet';
import { StreetGearsApiProvider } from '../providers/street-gears-api/street-gears-api';
import { LoginServiceProvider } from '../providers/login-service/login-service';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { LoginPage } from '../pages/login/login';
import { DbService } from '../providers/login-service/db.service';



export const firebaseConfig = {
  apiKey: "AIzaSyBSivQT8zqHuMpfVDAWN9ZmoAknSB3YCHY",
  authDomain: "projet-services-mobile.firebaseapp.com",
  databaseURL: "https://projet-services-mobile.firebaseio.com",
  projectId: "projet-services-mobile",
  storageBucket: "projet-services-mobile.appspot.com",
  messagingSenderId: "789530643074"
};

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    ComptePage,
    HistoriquePage,
    RecherchePage,
    ReserverTrajetPage,
    LoginPage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireDatabaseModule,
    AngularFireModule.initializeApp(firebaseConfig),
    HttpModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    ComptePage,
    HistoriquePage,
    RecherchePage,
    ReserverTrajetPage,
    LoginPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    StreetGearsApiProvider,
    LoginServiceProvider,
    AngularFirestore,
    AngularFireAuth,
    DbService
  ]
})
export class AppModule {}
